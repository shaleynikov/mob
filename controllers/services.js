/**
 * Created by admin on 12/10/2014.
 */

'use strict';

/* Services */

var responsiveServices = angular.module('responsiveServices', ['ngResource']);

responsiveServices.factory('Auction', ['$resource',
    function($resource){
        var res = $resource('http://aumt.edvantis.com/api/auctions/:auctionId', {}, {
            query: {method:'GET',params:{auctionId:''},isObject:true}
        });
        return res;
    }]);

responsiveServices.factory('Item', ['$resource',
    function($resource){
        var res = $resource('http://aumt.edvantis.com/api/auctions/:auctionId/items/:itemId', {}, {
            query: {method:'GET',params:{auctionId:'', itemId:''},isObject:true}
        });
        return res;
    }]);

responsiveServices.factory('Bid', ['$resource',
    function($resource){
        var res = $resource('http://aumt.edvantis.com/api/auctions/:auctionId/items/:itemId/bid', {}, {
            query: {method:'POST',params:{auctionId:'', itemId:''},isObject:true}
        });
        return res;
    }]);