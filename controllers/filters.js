/**
 * Created by admin on 12/10/2014.
 */

'use strict';

/* Filters */

angular.module('responsiveFilters', []).filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '\u2718';
    };
}).filter('okno', function() {
    return function(input) {
        return input ? 'Ok' : 'No';
    };
});
