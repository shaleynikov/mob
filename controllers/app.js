/**
 * Created by admin on 12/10/2014.
 */
'use strict';

/* App Module */
var gAuctionId = 0;
var gCurrentItemId = 0;

var responsiveApp = angular.module('responsiveApp', [
    'ngRoute',
    'responsiveControllers',
    'responsiveFilters',
    'responsiveServices',
    'angular-carousel'
]);

 responsiveApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/items', {
                templateUrl: 'views/item-list.html',
                controller: 'ItemListCtrl'
            }).
            when('/items/:itemId', {
                templateUrl: 'views/item-detail.html',
                controller: 'ItemDetailCtrl'
            }).
            when('/login',{
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            }).
            otherwise({
                redirectTo: '/login'
            });
    }
 ]);
