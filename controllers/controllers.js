/**
 * Created by admin on 12/10/2014.
 */

'use strict';

/* Controllers */

var responsiveControllers = angular.module('responsiveControllers', []);

responsiveControllers.controller('LoginCtrl', ['$scope', '$location', '$http',
    function($scope, $location, $http) {
        var isLogin = localStorage.getItem('isLogin', 0);
        if ( isLogin == 1 ) {
            $location.path('/items');
        }

        $scope.username = '';
        $scope.password = '';

        $scope.doLogin = function(){
            if ( $scope.username == '' || $scope.password == '' || $scope.auction_id == '' )
            {
                alert("Input username and password");
            }
            else
            {
                $http.get('http://aumt.edvantis.com/api/users?authenticate=true&username='+$scope.username+'&password='+$scope.password).
                    success(function(data, status, headers, config) {
                        if ( data.result == 'success' ){
                            var user_data = data.data;
                            localStorage.setItem('isLogin', 1);
                            localStorage.setItem('loginuser_id', user_data.id);
                            localStorage.setItem('auction_id', $scope.auction_id);
                            $location.path('/items');
                        }
                        else
                        {
                            localStorage.setItem('isLogin', 0);
                            alert(data.message);
                        }
                    }).
                    error(function(data, status, headers, config) {
                        console.log('login error', data, status);
                    });
            }
        }
    }
]);

responsiveControllers.controller('ItemListCtrl', ['$scope', '$sce', 'Item', '$location',
    function($scope, $sce, Item, $location) {
        var auction_id = localStorage.getItem('auction_id');
        Item.get({auctionId:auction_id}, function(returnData){
            if ( returnData.result == 'success' )
            {
                $scope.items = returnData.data;
                var auction_desc = $scope.items[0].auction_description;
                $scope.auction_description = $sce.trustAsHtml(auction_desc);
                /*
                angular.forEach($scope.items, function(value, key){
                    Item.get({itemId:value.id}, function(returnData){
                        if ( returnData.result == 'success' )
                        {
                            var itemData = returnData.data;
                            $scope.items[key].images = itemData.images;
                        }
                        else
                        {
                            alert(returnData.message);
                        }
                    });
                });
                */
            }
            else
            {
                alert(returnData.message);
                localStorage.setItem('isLogin', 0);
                $location.path('/login');
            }
        });

        $scope.getRemainingTime = function(date){
            try {
                var now = Math.ceil(Number(new Date()) / 1000),
                    dateTime = Math.ceil(Number(new Date(date)) / 1000),
                    diff = dateTime - now,
                    str;

                if (diff < 60) {
                    return String(diff);
                } else if (diff < 3600) {
                    str = String(Math.ceil(diff / (60)));
                    return str + (str == "1" ? ' minute' : ' minutes');
                }else if (diff < 86400) {
                    str = String(Math.ceil(diff / (3600)));
                    return str + (str == "1" ? ' hour' : ' hours');
                } else if (diff < 60 * 60 * 24 * 365) {
                    str = String(Math.ceil(diff / (60 * 60 * 24)));
                    return str + (str == "1" ? ' day' : ' days');
                } else {
                    return Date.format(new Date(date), 'H:m jS M Y');
                }
            } catch (e) {
                return '';
            }
        };

        $scope.logout = function(){
            localStorage.setItem('isLogin', 0);
            $location.path('/login');
        }
    }
]);

responsiveControllers.controller('ItemDetailCtrl', ['$scope', '$sce', '$location', '$http', '$routeParams', 'Item',
    function($scope, $sce, $location, $http, $routeParams, Item) {
        var auction_id = localStorage.getItem('auction_id');
        $scope.item = null;
        Item.get({auctionId:auction_id, itemId: $routeParams.itemId}, function(returnData) {
            if ( returnData.result == 'success' )
            {
                var itemLinks = returnData.links;
                for ( var i in itemLinks )
                {
                    var item = itemLinks[i];
                    if ( item.rel == 'next' )
                    {
                        var next_link = item.href,
                            next_link_items = next_link.split("/"),
                            next_item_id = next_link_items[next_link_items.length-1];
                        localStorage.setItem('next_item_id', next_item_id);
                    }
                    else if ( item.rel == 'prev' )
                    {
                        var prev_link = item.href,
                            prev_link_items = next_link.split("/"),
                            prev_item_id = prev_link_items[prev_link_items.length-1];
                        localStorage.setItem('prev_item_id', prev_item_id);
                    }
                }

                var itemData = returnData.data;
                console.log(itemData);
                var bid_history = [];
                for( var i in itemData.bid_history ){
                    bid_history.push(itemData.bid_history[i]);
                    if ( i > 9 )
                        break;
                }
                itemData.bid_history = bid_history;

                if ( itemData.images.length > 0 ){
                    for ( var i in itemData.images ){
                        itemData.images[i].id = i;
                    }
                }
                $scope.item = itemData;

                $scope.item_description = $sce.trustAsHtml(itemData.item_description);
                $scope.carouselIndex = 1;
            }
            else
            {
                alert(returnData.message);
                localStorage.setItem('isLogin', 0);
                $location.path('/login');
            }
        });

        $scope.getRemainingTime = function(date){
            try {
                var now = Math.ceil(Number(new Date()) / 1000),
                    dateTime = Math.ceil(Number(new Date(date)) / 1000),
                    diff = dateTime - now,
                    str;

                if (diff < 60) {
                    return String(diff);
                } else if (diff < 3600) {
                    str = String(Math.ceil(diff / (60)));
                    return str + (str == "1" ? ' minute' : ' minutes');
                }else if (diff < 86400) {
                    str = String(Math.ceil(diff / (3600)));
                    return str + (str == "1" ? ' hour' : ' hours');
                } else if (diff < 60 * 60 * 24 * 365) {
                    str = String(Math.ceil(diff / (60 * 60 * 24)));
                    return str + (str == "1" ? ' day' : ' days');
                } else {
                    return Date.format(new Date(date), 'H:m jS M Y');
                }
            } catch (e) {
                return '';
            }
        };

        $scope.getFormatTime = function(date){
            try {
                var now = Math.ceil(Number(new Date()) / 1000),
                    dateTime = Math.ceil(Number(new Date(date)) / 1000),
                    diff = now - dateTime,
                    str;

                if (diff < 60) {
                    return String(diff) + ' seconds ago';
                } else if (diff < 3600) {
                    str = String(Math.ceil(diff / (60)));
                    return str + (str == "1" ? ' minute' : ' minutes') + ' ago';
                }else if (diff < 86400) {
                    str = String(Math.ceil(diff / (3600)));
                    return str + (str == "1" ? ' hour' : ' hours') + ' ago';
                } else if (diff < 60 * 60 * 24 * 365) {
                    str = String(Math.ceil(diff / (60 * 60 * 24)));
                    return str + (str == "1" ? ' day' : ' days') + ' ago';
                } else {
                    return Date.format(new Date(date), 'H:m jS M Y');
                }
            } catch (e) {

                return '';
            }
        };

        $scope.activeSections = {bid_info:'active',description:'',bid_history:'',terms:''};
        $scope.bid_info_active = 'active';
        $scope.description_active = '';
        $scope.bid_history_active = '';

        //check if the tab is active
        $scope.isOpenSection = function (section) {
            //check if this tab is already in the activeTabs array
            if($scope.activeSections[section] == 'active') {
                //if so, return true
                return true;
            } else {
                //if not, return false
                return false;
            }
        };

        //function to 'open' a tab
        $scope.openSection = function (section) {
            //check if tab is already open
            if ($scope.isOpenSection(section)) {
                //if it is, remove it from the activeTabs array
                $scope.activeSections[section] = '';
            } else {
                //if it's not, add it!
                $scope.activeSections[section] = 'active';
            }
        };

        $scope.isShowLargeSlider = false;

        $scope.closeLargeSlider = function(){
            $scope.isShowLargeSlider = false;
        };

        $scope.carousel1Index = 0;
        $scope.carouselIndex2 = 0;

        $scope.openLargeSlider = function(index){
            if ( !$scope.swipe ) {
                $scope.isShowLargeSlider = true;
                $scope.carouselIndex2 = index;
            }

            $scope.swipe = false;
        };

        $scope.swipeSlider = function(swipe){
            $scope.swipe = true;
        };

        $scope.goNextItem = function(){
            var next_id = localStorage.getItem('next_item_id', '');
            if ( next_id ) {
                $location.path('/items/' + next_id);
            }
            else{
                alert('No Next Item');
            }

        };

        $scope.goPrevItem = function(){
            var prev_id = localStorage.getItem('prev_item_id', '');
            if ( prev_id ){
                $location.path('/items/'+prev_id);
            }
            else{
                alert('No prev item');
            }
        };

        $scope.isShowBidDlg = false;
        $scope.isShowConfirmDlg = false;
        $scope.bid_price = '';

        $scope.openBidDlg = function(){
            $scope.isBidError = false;
            $scope.isBidSuccess = false;
            this.bid_price = '';
            $scope.isShowBidDlg = true;
        };

        $scope.closeBidDlg = function(){
            this.bid_price = '';
            $scope.isShowBidDlg = false;
            $scope.success_message = '';
            $scope.error_message = '';
            $scope.isBidResult = false;
        };

        $scope.doConfirm = function(){
            if ( this.bid_price == '' || this.bid_price == 0 )
            {
                alert("Please input bid amount");
            }
            else
            {
                $scope.isShowBidDlg = false;
                $scope.isShowConfirmDlg = true;
                this.check_terms = false;
            }
        };

        $scope.closeConfirmDlg = function(){
            $scope.isShowConfirmDlg = false;
            $scope.isShowBidDlg = true;
        };

        $scope.closeResultDlg = function(){
            $scope.isShowResultDlg = false;
        };

        $scope.check_terms = false;

        $scope.success_message = '';
        $scope.isBidSuccess = false;
        $scope.error_message = '';
        $scope.isBidError = false;
        $scope.isShowResultDlg = false;

        $scope.doBid = function(){
            if ( !this.check_terms )
            {
                alert("Please check to agree terms and conditions");
                return false;
            }

            var auction_id = localStorage.getItem('auction_id', 0);
            var item = $scope.item;
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
            $http.post('http://aumt.edvantis.com/api/auctions/'+auction_id+'/items/'+item.id+'/bid' , $.param({bid: this.bid_price})).
                success(function(returnData){
                    console.log(returnData);
                    if ( returnData.result == 'success' )
                    {
                        //alert(returnData.message)
                        $scope.success_message = $sce.trustAsHtml(returnData.message);
                        $scope.isBidSuccess = true;
                        $scope.error_message = '';
                        $scope.isBidError = false;

                        Item.get({auctionId:auction_id, itemId: $routeParams.itemId}, function(returnData) {
                            if ( returnData.result == 'success' )
                            {
                                var itemData = returnData.data;
                                console.log('SUCCESSBID', itemData);
                                var bid_history = [];
                                for( var i in itemData.bid_history ){
                                    bid_history.push(itemData.bid_history[i]);
                                    if ( i > 9 )
                                        break;
                                }
                                itemData.bid_history = bid_history;

                                if ( itemData.images.length > 0 ){
                                    for ( var i in itemData.images ){
                                        itemData.images[i].id = i;
                                    }
                                }
                                $scope.item = itemData;
                            }
                            else
                            {
                                alert(returnData.message);
                                localStorage.setItem('isLogin', 0);
                                $location.path('/login');
                            }
                        });
                    }
                    else{
                        $scope.error_message = $sce.trustAsHtml(returnData.message);
                        $scope.success_message = '';
                        $scope.isBidSuccess = false;
                        $scope.isBidError = true;
                    }
                    $scope.isShowConfirmDlg = false;
                    $scope.isShowResultDlg = true;
                }).
                error(function(data){
                    alert(data);
                });
        };

        $scope.logout = function(){
            localStorage.setItem('isLogin', 0);
            $location.path('/login');
        };
    }
]);